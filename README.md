# Issue Tracker for the Fedora Bootable Containers initiative

This repo is only used to track [issues](https://gitlab.com/fedora/bootc/tracker/-/issues).

## Links

See:
- [Fedora Initiative](https://fedoraproject.org/wiki/Initiatives/Fedora_bootc)
  - [Announcement on the Fedora Magazine](https://fedoramagazine.org/get-involved-with-fedora-bootable-containers/)
  - [Discussion about this initiative](https://discussion.fedoraproject.org/t/fedora-council-tickets-ticket-492-new-initiative-fedora-bootc/116062)
- [Discussion on Fedora's Discussion forums](https://discussion.fedoraproject.org/tag/bootc-initiative)
- [#bootc:fedoraproject.org Matrix room](https://matrix.to/#/#bootc:fedoraproject.org)
- [Documentation](https://docs.fedoraproject.org/en-US/bootc/)

## Meetings

Folks working on this initiative are meeting weekly on:

- Tuesday, [15:00 UTC](https://time.is/14:00+UTC)

Meetings usually happen via text chat on Matrix in the
[#meeting-1 Matrix channel](https://matrix.to/#/#meeting-1:fedoraproject.org).

Sometimes, we make schedule video meetings and those will take place in
[meet.google.com/poh-xmxm-qyc](https://meet.google.com/poh-xmxm-qyc)

See the following calendar entries in Fedora's Calendar:

- <https://calendar.fedoraproject.org/SIGs/2025/1/27/#m10982>

The notes are written in
[an ephemeral etherpad](https://etherpad.opensuse.org/p/bootc-initiative-meetings)
and then made available in the [meeting-notes](meeting-notes) folder in this
repo.
