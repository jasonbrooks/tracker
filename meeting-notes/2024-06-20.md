# bootable containers initiative meeting - 2024-06-20

## Attendees

- Timothée Ravier
- Hristo Marinov
- Colin Walters
- Noel Miller
- Robert Sturla
- Jonathan Lebon
- Jason Brooks
- Josh Gwosdz

## Agenda

https://gitlab.com/fedora/bootc/tracker/-/issues/?label_name%5B%5D=Meeting 

## Decide who will be writting notes and who will make the meeting notes PR

- Jonathan

## ostree archive's mtime modification results in slower python execution

- CW: non-trivial lift for this. still in background.

## Meeting frequency

- CW: Discussing lowering frequency to once a week, continuing to discuss async
- NM: Agreed. also maybe 30m is too short. also gives more time between meetings for things to get done
- JB: 1h tuesday? text vs video?
- NM: maybe 3 times a month text, and 1 time video
- JL: mix makes sense to me!
- CW: go to once a week
- NM: if we keep it at 30m, then we need to be focused and prepared for the topic we want to discuss

## Anaconda

- https://gitlab.com/fedora/bootc/tracker/-/issues/20 
- NM: a lot of things need to be done in e.g. %post which is hacky. no progress bar.
- CW: progress bar for deployment needs some work on the bootc/ostree side, defining an API; xref https://github.com/containers/bootc/issues/522 and 
- CW: b-i-b supports creating custom Anaconda live ISO to automate
- CW: kickstart doesn't support target imgref (when you have different source and "remote" imgref)
- JK: we don't have the source for the progress bar. missing support from the tools we use. with switch to bootc, do we want to spend time on this?
- JK: re. ISO with UI support, Anaconda should detect when you want the container flow and give you the GUI for that purpose. Right now, Anaconda tries to guess what "kind" of install it is based on what's on the ISO.
- JK: can have kickstart dropin that sets interactive mode we want.
- JK: ideally, we'd use configuration files but limited capacity. 
- NM: yeah, looks like configuration files is the future but not yet fully used. for ublue, we moved to an offline install flow because pulling over the internet there's no retry mechanism.
- CW: putting OCI archive in the ISO is the dumb way to do it. we want to deduplicate with the ANaconda ISO using composefs e.g.
- JL: could make a derived container build that adds Anaconda, and then build the Anaconda ISO out of that

    - TR: https://github.com/travier/fedora-kinoite/blob/main/fedora-kinoite-live/Containerfile

- CW: Anaconda works from a squashfs. With composefs, we can maintain the files separately from the squashfs. So we can get multiple images that share the backing files. The composefs image is basically just
metadata. The composefs image you boot from and the image you install from can be different but will still share storage.
- JK: seems great. this will be helpful even with current Anaconda. erofs supports layering, so you can use it to just deploy one of the layers?
- CW: no. composefs = overlayfs + erofs + fs-verity. overlayfs now has data only lowerdirs. e.g. Anaconda's "Verify my ISO" would kinda be obsolete by composefs.
- NM: definitely a lack of supporting staff to work on Anaconda. there's a need there if we want to get some of these problems solved. need more open-source contributors to help out.
- CW: do a synchronized rollup of what we all did. have release notes. would be a good way to build momentum.
