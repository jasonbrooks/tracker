# bootable containers initiative meeting - 2024-10-08

## Attendees

- Jason Brooks
- Robert Sturla
- Joseph Gayoso
- Eric Curtin

## Agenda

https://gitlab.com/fedora/bootc/tracker/-/issues/?label_name%5B%5D=Meeting

## Decide who will be writting notes and who will make the meeting notes PR

- Jason

## Joseph on marketing

- targetting social media

- It'd be good to have an article about bootc, to be pubbed around f41 release
- Jason will follow up on this

- Each of the editions wants to have an article
- IoT and bootc?
- Atomic desktops

## Next week

###  bootc This Week

- last Fri edition:
- https://discussion.fedoraproject.org/t/bootc-this-week-2024-10-04/132957
- docs on using it:
- https://discussion.fedoraproject.org/t/writing-a-good-bootc-this-week-entry/132815
- issue to move config to our repo:
- https://gitlab.com/fedora/bootc/tracker/-/issues/41