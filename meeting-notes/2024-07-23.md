# bootable containers initiative meeting - 2024-07-23

## Attendees

- Timothée Ravier
- Paul Whalen
- Colin Walters
- Renata Ravanelli
- David Cantrell 
- Hristo Marinov
- JB Trystram
- Jason Brooks
- Joseph Marrero
- Jonathan Lebon

## Agenda

https://gitlab.com/fedora/bootc/tracker/-/issues/?label_name%5B%5D=Meeting

## dnf & bootc (warning messages)

- https://github.com/rpm-software-management/dnf/pull/2110
- https://github.com/rpm-software-management/dnf/pull/2112 

## Local package layering

- https://gitlab.com/fedora/bootc/tracker/-/issues/4
- Phases:
    - https://gitlab.com/fedora/bootc/tracker/-/issues/4#note_1931202541
