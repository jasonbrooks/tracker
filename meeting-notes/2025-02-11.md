```
=====================================
# #meeting-1:fedoraproject.org: fedora_bootc_meeting
=====================================

Meeting started by @jbrooks:matrix.org at 2025-02-11 15:00:34



Meeting summary
---------------
* TOPIC: roll call (@jbrooks:matrix.org, 15:00:46)
* TOPIC: https://github.com/containers/buildah/issues/5952 (@walters:fedora.im, 15:05:40)

Meeting ended at 2025-02-11 15:34:36

Action items
------------

People Present (lines said)
---------------------------
* @walters:fedora.im (20)
* @dustymabe:matrix.org (18)
* @jbrooks:matrix.org (11)
* @zodbot:fedora.im (11)
* @nalind:matrix.org (6)
* @rsturla:fedora.im (4)
* @jlebon:fedora.im (4)
* @siosm:matrix.org (3)
* @meetbot:fedora.im (2)
* @mheon:matrix.org (1)
* @hricky:fedora.im (1)
* @jbtrystram:matrix.org (1)
* @bbaude:matrix.org (1)
* @jmarrero:matrix.org (1)
```

[full meeting log here](https://meetbot.fedoraproject.org/meeting-1_matrix_fedoraproject-org/2025-01-28/fedora-bootc-initiative.2025-01-28-15.00.log.html)