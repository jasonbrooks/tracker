# bootable containers initiative meeting - 2024-08-27

This etherpad is ephemeral and will be cleaned-up between each meeting.
The Notes will end up in https://gitlab.com/fedora/bootc/tracker

## Attendees

- Hristo Marinov
- Jason Brooks
- Renata Andrade
- Timothée Ravier
- Jonathan Lebon
- Robert Sturla
- Joseph Marrero
- 

## Agenda

https://gitlab.com/fedora/bootc/tracker/-/issues/?label_name%5B%5D=Meeting

## Decide who will be writting notes and who will make the meeting notes PR

- ?

## Fedora 41 bootc images

- Status?
- https://gitlab.com/fedora/bootc/base-images/-/merge_requests/35 has been merged

## Atomic desktops pushing composefs to f42

- Relies on bootupd, which is just arriving in f41
- https://gitlab.com/fedora/ostree/sig/-/issues/1#note_2062751164

## CI & Konflux

- https://gist.github.com/ralphbean/a3644111a549e8cedb0b207f90d42dc9#file-readme-md
